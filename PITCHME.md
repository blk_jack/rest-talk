#HSLIDE
### Client first APIs with mock data, Retrofit and Swagger

#HSLIDE
### Client first?  Mock data?  Retrofit and "Swagger"?!?1
- App first, no backend, using "loose' requirements. Mock-ups/storyboards.
- Mock data is actually hardcoded JSON assets
- Retrofit is a REST http library developed by Square
- Swagger is the world's "most popular" API platform

![Swagger](images/logo_swagger.jpg)
![Square](images/logo_square.jpg)

#HSLIDE
### Who is this for?
- Independent developers <!-- .element: class="fragment" -->
- Small teams <!-- .element: class="fragment" -->
- Anybody dealing with bad project managers <!-- .element: class="fragment" -->
    - Seriously, protect your developers <!-- .element: class="fragment" -->
- You! <!-- .element: class="fragment" -->

#HSLIDE
### What can wrong?
- Requirement specs pre-date mock-ups/design
- Existing backend needs heavy modifications to match new app/scope
- Backend changes become a blocker

#HSLIDE
![Labs](images/background_labs.png)

#HSLIDE
![Creepz](images/feature_creep.jpg)

#HSLIDE
### Starting over
- No backend! <!-- .element: class="fragment" -->
- No requirements !? <!-- .element: class="fragment" -->
- Small team <!-- .element: class="fragment" -->
- No existing software integration <!-- .element: class="fragment" -->

#HSLIDE
![MockUp](images/background_mockup.jpg)

#HSLIDE
![ListMockUp](images/background_mockup_list.png)

#HSLIDE
### Retrofit!  Also OkHttp!
![ListMockUp](images/wut_is_retrofit_1.png) <!-- .element: class="fragment" -->
![ListMockUp](images/wut_is_retrofit_2.png) <!-- .element: class="fragment" -->

#HSLIDE
### OkHttp Interceptors
Interceptors are a powerful mechanism that can __monitor__, __rewrite__, and __retry__ calls. 

#HSLIDE
### POJO
```java
public class ListItem {
    private long id;
    private int type;
    private String title;
    private long timestamp;
    private String imageUrl;

    public long getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
```

#HSLIDE
### Endpoint interface
```java


public interface ListApi {
    @GET("/list")
    Observable<List<ListItem>> getList(@Query("page") Integer page);
}


```

#HSLIDE
### Mock Interceptor for OkHttp
```java
public class MockInterceptor implements Interceptor {

    private Context context;

    public MockInterceptor(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        // Get resource ID for mock response file.
        String fileName = getFilename(chain.request());
        int resourceId = getResourceId(fileName);
        if (resourceId == 0) {
            throw new IOException("Could not find res/raw/" + fileName);
        }

        // Get input stream and mime type for mock response file.
        InputStream inputStream = context.getResources().openRawResource(resourceId);
        String mimeType = URLConnection.guessContentTypeFromStream(inputStream);
        if (mimeType == null) {
            mimeType = "application/json";
        }

        // Build and return mock response.
        return new Response.Builder()
                .addHeader("content-type", mimeType)
                .body(ResponseBody.create(MediaType.parse(mimeType), toByteArray(inputStream)))
                .code(200)
                .message("Mock response from res/raw/" + fileName)
                .protocol(Protocol.HTTP_1_0)
                .request(chain.request())
                .build();
    }

    private String getFilename(Request request) throws IOException {
        String requestedMethod = request.method();
        String filename = requestedMethod + request.url().url().getPath();
        filename = filename.replace("/", "_").replace("-", "_").toLowerCase();
        return filename;
    }

    private int getResourceId(String filename) {
        return context.getResources().getIdentifier(filename, "raw", context.getPackageName());
    }

    private static byte[] toByteArray(InputStream is) throws IOException {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] b = new byte[4096];
            int n;
            while ((n = is.read(b)) != -1) {
                output.write(b, 0, n);
            }
            return output.toByteArray();
        }
    }
}


```

#HSLIDE
### Inject Retrofit and OkHttp
```java
@Provides @Singleton
OkHttpClient provideOkHttpClient(Context context, Cache cache) {
    OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
            .cache(cache);
    if (BuildConfig.BUILD_TYPE.equals("debug")) {
        MockResponseInterceptor mock = new MockResponseInterceptor(context);
        builder.addInterceptor(chain -> {
            Log.d("MOCK REQUEST [%s]", chain.request().url());
            return chain.proceed(chain.request());
        });
        builder.addInterceptor(mock);
    }
    return builder.build();
}

@Provides @Singleton
Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
    return new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .baseUrl(Constants.API_BASE_URL)
            .client(okHttpClient)
            .build();
}

@Provides @Singleton
ListApi providesListApi(Retrofit retrofit) {
    return retrofit.create(ListApi.class);
}


```

#HSLIDE
### JSON mock data

```
[
  {
    "id": 0,
    "timestamp": 1479927259000,
    "type": 0,
    "title": "Google Announces the Google Play Indie Games Contest in Europe",
    "image_url": "https://www1-lw.xda-cdn.com/files/2016/11/Indie-Games-810x298_c.png"
  },
  {
    "id": 1,
    "timestamp": 1479920449000,
    "type": 0,
    "title": "OnePlus 3T XDA Review: What Has Changed, and by How Much",
    "image_url": "https://www1-lw.xda-cdn.com/files/2016/11/op3treview-810x298_c.png"
  },
  {
    "id": 2,
    "timestamp": 1478523600000,
    "type": 1,
    "title": "Beta Test EMUI 5.0 (Nougat) on the Honor 8",
    "description": "Ut consectetur eu laboris ipsum tempor pariatur ea esse anim nisi.",
    "image_url": "https://img.xda-cdn.com/7ki2Wqosuoud7Fh_0uIzBKS7oMs=/http%3A%2F%2Fwww1-lw.xda-cdn.com%2Ffiles%2F2016%2F11%2Fnougat2.jpg"
  },
  {
    "id": 3,
    "timestamp": 1479729767000,
    "type": 0,
    "title": "Google Pixel XL XDA Review: A Foundational Release for Google & Post-Nexus Android",
    "description": "Ut consectetur eu laboris ipsum tempor pariatur ea esse anim nisi.",
    "image_url": "https://www1-lw.xda-cdn.com/files/2016/11/pixelfront2-810x298_c.png"
  },
  {
    "id": 4,
    "timestamp": 1479729764800,
    "type": 0,
    "title": "Which Current or Upcoming Processor Would You Want in Your 2017 Device?",
    "description": "Ex reprehenderit ut non velit officia aute esse anim ex fugiat reprehenderit.",
    "image_url": "https://www1-lw.xda-cdn.com/files/2016/11/discussprocessor-810x298_c.png"
  }
]


```


#HSLIDE
### All together now
**OkHttpClient** instantiated with **MockIntereptor** to use resource JSON files instead of network requests.

**Retrofit** instantiated with **OkHttpClient**.

#HSLIDE
![MockUp](images/background_mockup.jpg)

#HSLIDE
<video data-video="true" class="stretch" data-autoplay="" src="http://mimic.ca/~jeff/talks/list_scroll.mp4" style="height: 100%; width: 100%;" googl="true" loop="true"></video>

#HSLIDE
### Swagger
- Full set of tools to design, build and document RESTful APIs
- Three main components -- Swagger Editor, Swagger Codegen, Swagger UI
- Open source licensed under Apache 2.0
- Swagger toolbox: https://swagger-toolbox.firebaseapp.com/

#HSLIDE?image=images/swagger_toolbox.png

#HSLIDE?image=images/swagger_editor.png

#HSLIDE?image=images/swagger_editor_faker.png

#HSLIDE?image=images/json_schema_faker.png

#HSLIDE
### JSON Generated from Swagger schema
```json
[
  {
    "id": 5867,
    "timestamp": 1479927454395,
    "title": "Nulla maxime quas ut molestiae corrupti accusamus repellendus eum.",
    "description": "Et suscipit earum dolores magnam vitae.\nRatione sed dolor veritatis blanditiis et.\nQuos maxime necessitatibus molestias nihil modi.",
    "image_url": "http://lorempixel.com/640/480"
  },
  {
    "id": 89340,
    "timestamp": 1479928432584,
    "title": "Soluta nesciunt aut hic architecto et non repellendus alias expedita.",
    "description": "Aut quia ad dolorem et qui enim quis.\nNon magni sit dolor voluptatem omnis eius.\nCumque rerum repellendus aperiam qui.",
    "image_url": "http://lorempixel.com/640/480"
  },
  {
    "id": 51490,
    "timestamp": 1479927559337,
    "title": "Sapiente et quo aspernatur.",
    "description": "Soluta non eius quia odio ut numquam dignissimos fugit.\nItaque ea pariatur dolores et expedita velit voluptas.\nVoluptatem occaecati sunt consequatur fuga quaerat doloremque tenetur.",
    "image_url": "http://lorempixel.com/640/480"
  },
  {
    "id": 18645,
    "timestamp": 1479928853103,
    "title": "At in quos porro sit nihil dolorem.",
    "description": "Nam possimus eum.\nEnim qui id.\nSed labore deleniti iusto ipsum similique sit cupiditate.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 7
  },
  {
    "id": 89099,
    "timestamp": 1479927680207,
    "title": "Architecto fugiat debitis explicabo aperiam veniam ut nihil dolorum.",
    "description": "Incidunt in similique officia magnam vitae sit.\nEa sed voluptatem sequi et aut unde quo eaque quibusdam.\nMolestiae optio odit iusto et sunt velit quia deserunt suscipit.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 1
  },
  {
    "id": 45414,
    "timestamp": 1479928456167,
    "title": "Porro numquam est id sed consequuntur error.",
    "description": "Laboriosam ut et sit.\nNatus repudiandae aut velit fugit soluta.\nEius consequatur dignissimos laboriosam.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 1
  },
  {
    "id": 81724,
    "timestamp": 1479928492976,
    "title": "Labore fuga minima voluptatem et sunt id nemo animi qui.",
    "description": "Est perspiciatis harum placeat non aut dolores vero.\nOfficia adipisci autem illo aperiam aliquid id dicta pariatur.\nEos ipsum sunt id ut nihil voluptates et laudantium.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 2
  },
  {
    "id": 21214,
    "timestamp": 1479928205496,
    "title": "Et voluptatibus veniam reiciendis sit dicta unde voluptates quos.",
    "description": "Harum numquam pariatur.\nAlias voluptas ipsa.\nEt iure fugit dolor quaerat iste non expedita soluta ipsa.",
    "image_url": "http://lorempixel.com/640/480"
  },
  {
    "id": 78170,
    "timestamp": 1479927702152,
    "title": "A quidem quia nemo sed.",
    "description": "Laudantium minima aut ipsum soluta.\nEst sequi et tempora.\nSoluta illum doloribus excepturi eaque molestias.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 2
  },
  {
    "id": 30475,
    "timestamp": 1479927856956,
    "title": "Nobis ex quia dolores nostrum hic voluptatibus fugiat quam.",
    "description": "Deleniti quo accusamus earum dolore qui.\nFacilis fugiat asperiores voluptate iure tempore recusandae sed non voluptas.\nAssumenda aut omnis et et mollitia aut accusamus dolores.",
    "image_url": "http://lorempixel.com/640/480",
    "type": 4
  }
]

```

#HSLIDE
### Swagger Codegen
The Swagger Codegen is an open source code-generator to build server stubs and client SDKs directly from a Swagger defined RESTful API
- CLI tool from github
- Web GUI directly from Swagger Editor
- Over 20 different server languages
- Over 40 different client SDKs

#HSLIDE?image=images/swagger_editor_client.png

#HSLIDE
### Generate an Android SDK using OkHttp and Gson
```

mvn clean package
java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
  -i http://petstore.swagger.io/v2/swagger.json \
  -l java --library=okhttp-gson \
  -D hideGenerationTimestamp=true \
  -o /var/tmp/java/okhttp-gson/ 
```

#HSLIDE
### Swagger UI
Swagger UI allows anyone — be it your development team or your end consumers — to visualize and interact with the API’s resources without having any of the implementation logic in place. It’s automatically generated from your Swagger specification, with the visual documentation making it easy for back end implementation and client side consumption.


#HSLIDE?image=images/swagger_ui_spec.png

#HSLIDE?image=images/swagger_ui_expanded.png

#HSLIDE?image=images/django_rest_swagger.png

#HSLIDE?image=images/swagger_in_django.png

#HSLIDE
### Cli3nt f1r5t AP1s wi7h m0ck d4ta, Re7r0f1t and Sw4gg3r
- MockInterceptor added to OkHttp
- Retrofit instantiated with OkHttp + Interceptor
- JSON files in app
- Swagger spec built from JSON
- Swagger codegen to generate backend
- Swagger UI built into backend


#HSLIDE
<video data-video="true" class="stretch" data-autoplay="" src="http://mimic.ca/~jeff/talks/feed_0.10.mp4" style="height: 100%; width: 100%;" googl="true" loop="true"></video>
